import time
from twisted.application.service import Application
from twisted.application.internet import TimerService, TCPServer
from twisted.web import server, resource, static
from twisted.python import log

from scrapyd.interfaces import IEggStorage, IPoller, ISpiderScheduler, IEnvironment
from scrapyd.launcher import Launcher
from scrapyd.eggstorage import FilesystemEggStorage
from scrapyd.scheduler import SpiderScheduler
from scrapyd.environment import Environment

from scrapyd.website import Root, Home, ProcessMonitor
from scrapyd import webservice
from scrapyd.config import Config

from scrapy.conf import settings
from scrapy import log

from twisted.web.wsgi import WSGIResource
from twisted.internet import reactor
from twisted.python.threadpool import ThreadPool
from twisted.application.service import IServiceCollection

from .poller import QueuePoller

def application(config):
    app = Application("Scrapyd")
    http_port = config.getint('http_port', 6800)
    poller = QueuePoller(config)
    
    eggstorage = FilesystemEggStorage(config)
    scheduler = SpiderScheduler(config)
    environment = Environment(config)

    app.setComponent(IPoller, poller)
    app.setComponent(IEggStorage, eggstorage)
    app.setComponent(ISpiderScheduler, scheduler)
    app.setComponent(IEnvironment, environment)

    launcher = Launcher(config, app)
    from scrapy.conf import settings
    timer = TimerService(settings.get('QUEUE_POLL_INTERVAL', 5), poller.poll)
    import sys
    try:
        del sys.modules['scrapy.conf']
    except KeyError:
        pass

    webservice = TCPServer(http_port, server.Site(DispatcherAwareRoot(config, app)))
    log.msg("Scrapyd web console available at http://localhost:%s/" % http_port)

    launcher.setServiceParent(app)
    timer.setServiceParent(app)
    webservice.setServiceParent(app)

    return app
