# Scrapy settings for crawler project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'crawler'
BOT_VERSION = '1.0'

SPIDER_MODULES = ['dance_event.crawler.spiders']
NEWSPIDER_MODULE = 'dance_event.crawler.spiders'
USER_AGENT = '%s/%s' % (BOT_NAME, BOT_VERSION)

COMMANDS_MODULE = 'dance_event.crawler.commands'

DEBUG = True

DEFAULT_SPIDERS = [

                   'o2cm',
                    'zsconcepts'
                   ]



SPIDER_MIDDLEWARES = {
    'subspider.middleware.SubspiderMiddleware': 10,
}

DOWNLOADER_MIDDLEWARES = {
    'dance_event.crawler.middleware.SpiderDefinedMiddleware':543,
}
SESSION_POOL_SIZE = 10

SUBSPIDER_HOST = 'http://localhost:6800/'
SUBSPIDER_SERVICE = 'schedule.json'
SUBSPIDER_DEFAULTS = {'project':'default'}
SUBSPIDER_DESTINATION = SUBSPIDER_HOST + SUBSPIDER_SERVICE

FEED_STORAGES = {'sqlite':'dance_event.crawler.feedexport.AlchemyDBStorage',
				 'postgres':'dance_event.crawler.feedexport.AlchemyDBStorage',
				 'mysql':'dance_event.crawler.feedexport.AlchemyDBStorage'} 

# FEED_URI = 'sqlite:///test.db'

FEED_FORMAT = 'sql'

FEED_EXPORTERS = {'sql':'dance_event.crawler.exporter.AlchemyItemExporter'}