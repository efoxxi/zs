import os
import uuid
import pickle
import time
from scrapy.contrib.feedexport import BlockingFeedStorage
from scrapy import log
from scrapy.item import Item
from scrapy.conf import settings

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import OperationalError

from multiprocessing.managers import BaseManager
from multiprocessing import Queue, Value, Event
from Queue import Queue as ItemQueue



from dance_event.crawler.models import Base
from dance_event.crawler.items import PersonResult

_managers = {}
class EnvvarConnectedManager(BaseManager):
    @classmethod
    def is_server(cls):
        return not all((os.environ.get(envvar_name, None) for envvar_name in (cls.address_env, cls.authkey_env)))

    def serve(self):
        server = self.get_server()
        self.pre_serve(server)
        os.environ[self.address_env] = pickle.dumps(server.address)
        os.environ[self.authkey_env] = pickle.dumps(r'%s'%server.authkey)
        server.serve_forever()

    def pre_serve(self, server):
        log.msg('Serving manager %s@%s'%([server.authkey,], server.address))
    
    def pre_connect(self):
        pass

    def __init__(self, serve, **kwargs):
        if not serve:
            self.pre_connect()
            super(EnvvarConnectedManager, self).__init__(**kwargs)
            self.connect()
            log.msg('Connected to manager %s'%self._address)
        else:
            super(EnvvarConnectedManager, self).__init__(**kwargs)
            self.serve()


class SessionManager(EnvvarConnectedManager):
    address_env = 'SCRAPY_SESSION_MANAGER_ADDRESS'
    authkey_env = 'SCRAPY_SESSION_MANAGER_AUTHKEY'
    base = Base

    def get_session(self, uri):
        self.tables_created(uri)
        self.created().wait()
        if not hasattr(self, '_session_pool'):
            self._session_pool = self.get_session_pool()
        self._session_pool.get()
        log.msg('Open session for %s'%uri)
        return sessionmaker(bind=create_engine(uri))()

_managers = {}
def manager(manager_class=SessionManager, address=None, authkey=None):
    serve = manager_class.is_server()
    address = address if address else pickle.loads(os.environ.get(manager_class.address_env, pickle.dumps(None)))
    authkey = authkey if authkey else pickle.loads(os.environ.get(manager_class.authkey_env, pickle.dumps(None)))

    if serve:
        session_pool = Queue(settings['SESSION_POOL_SIZE'])
        while not session_pool.full():
            session_pool.put(True)

        log.msg('Created %s database session slots'%settings['SESSION_POOL_SIZE'])

        created_uris = []
        created = Event()
        def tables_created(uri):
            if not uri in created_uris:
                log.msg('Creating schema at `%s`'%uri)
                session = sessionmaker(bind=create_engine(uri))()
                manager_class.base.metadata.create_all(session.bind)
                session.commit()
                session.close()
                created_uris.append(uri)
            created.set()
        
        manager_class.register('created', callable=lambda:created)
        manager_class.register('tables_created', callable=tables_created)
        manager_class.register('get_session_pool', callable=lambda:session_pool)
        manager_class.register('release', callable=lambda:session_pool.put(True))


    else:
        manager_class.register('created')
        manager_class.register('get_session_pool')
        manager_class.register('tables_created')
        manager_class.register('release')

    if not manager_class.__name__ in _managers:
        _managers[manager_class.__name__] = manager_class(serve,
                                                          address=address,
                                                          authkey=authkey)
        
    return _managers[manager_class.__name__]

class SessionDBStorage(BlockingFeedStorage):
    WD = 100
    INTERVAL = 0.01
    THROTTLE_ON_EXCEPTIONS = [OperationalError,]
    def __init__(self, uri):
        self.uri = uri

    def open(self, spider):
        if not hasattr(self, '_item_queue'):
            self._item_queue = ItemQueue()
        return self._item_queue

    def _store_in_thread(self, queue):
        mgr = manager()
        session = mgr.get_session(self.uri)
        # log.msg('pool size: %s'%mgr._session_pool.qsize())
        while not queue.empty():
            self._store(*queue.get()+(session,))
            wd = self.WD
            try:
                session.commit()
            except Exception, e:
                if not e.__class__ in self.THROTTLE_ON_EXCEPTIONS:
                    raise
                if not wd:
                    raise
                wd -= 1
                time.sleep(self.INTERVAL)

        session.close()
        mgr.release()

class AlchemyDBStorage(SessionDBStorage):
    omit_existance_check = [PersonResult,]
    def __init__(self, *args, **kwargs):
        super(AlchemyDBStorage, self).__init__(*args, **kwargs)
        self.exported = {}

    def _store(self, item, to_fields, session):
        if not hasattr(item, '_model'):
            return

        if item in self.exported:
            return self.exported[item]

        fields = to_fields(item)
        for name, rel in [(name, value) for name, value \
                        in fields.iteritems() if isinstance(value, Item)]:
            fields[name] = self._store(rel, to_fields, session)

        if not item.__class__ in self.omit_existance_check:
            existing_item = session.query(item._model).filter_by(**fields).first()
        else:
            existing_item = None

        orm_item = existing_item if existing_item else item._model(**fields)
        self.exported[item] = orm_item
        session.add(orm_item)
        return orm_item