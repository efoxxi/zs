from scrapyd.spiderqueue import SqliteSpiderQueue
from .sqlite import JsonSqlitePriorityQueue
class UnlockedSqliteSpiderQueue(SqliteSpiderQueue):
    def __init__(self, database=None, table='spider_queue'):
        self.q = JsonSqlitePriorityQueue(database, table)
