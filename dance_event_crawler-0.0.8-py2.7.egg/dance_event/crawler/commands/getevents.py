import os
import sys
import signal
from datetime import datetime, timedelta, date
from itertools import chain
import time
from json import loads
from urllib2 import urlopen
from urllib import urlencode

# from subprocess import Popen, PIPE
import multiprocessing
from scrapy import log
from scrapy.command import ScrapyCommand
from scrapy.utils.conf import arglist_to_dict
from scrapy.exceptions import UsageError
from scrapy.conf import settings
from scrapy.contrib.exporter import CsvItemExporter
from scrapyd.interfaces import ISpiderScheduler
from twisted.application.service import IServiceCollection

# os.environ.setdefault('SCRAPY_SETTINGS_MODULE', 'project.settings') #Must be at the top before other imports

from subspider import SpiderSerializer as subspider
from subspider.middleware import enqueue

class Scrapyd(multiprocessing.Process):
    def __init__(self, *args, **kwargs):
        self.started = multiprocessing.Event()
        super(Scrapyd, self).__init__(*args, **kwargs)

    def run(self):
        from twisted.python import log
        from twisted.application import app
        from twisted.internet import reactor
        from twisted.internet.error import CannotListenError
        from scrapyd.script import _get_config
        from scrapyd import get_application

        from threading import Thread
        from dance_event.crawler.feedexport import manager
        mgr = Thread(target=manager)
        mgr.start()


        config = _get_config()
        log.startLogging(sys.stderr)
        application = get_application(config)

        try:
            app.startApplication(application, False)
        except CannotListenError:
            self.terminate()
            raise

        
        stop = reactor.stop
        def stopall(self, *args):
            for sproto in IServiceCollection(application,
                                             application)\
                         .getServiceNamed('launcher').processes.values():
                os.kill(sproto.pid, 9)
            stop()
        reactor.stop = stopall.__get__(reactor, reactor.__class__)
        self.started.set()
        reactor.run()

    
DATE_FORMAT = "%Y-%m-%d"
class Command(ScrapyCommand):

    requires_project = True

    def syntax(self):
        return "[options] <spider>"

    def short_desc(self):
        return "Crawl as CSV data"

    def add_options(self, parser):
        ScrapyCommand.add_options(self, parser)
        parser.add_option("-a", dest="spargs", action="append", default=[], metavar="NAME=VALUE", \
            help="set spider argument (may be repeated)")
        parser.add_option("-o", "--output", metavar="FILE", \
            help="dump scraped items into FILE (use - for stdout)")
        parser.add_option("-t", "--output-format", metavar="FORMAT", default="csv", \
            help="format to use for dumping items with -o (default: %default)")

        parser.add_option("-p", "--pool", default=20, \
            help="Number of concurrent db sessions --pool (default: %(default)")

        parser.add_option("-s", "--since", default=None, \
            help="crawl events since date (default: %(default)")

        parser.add_option("-u", "--until", default=datetime.now(), \
            help="crawl events until date (default: %(default)")

    def process_options(self, args, opts):
        ScrapyCommand.process_options(self, args, opts)
        try:
            opts.spargs = arglist_to_dict(opts.spargs)
        except ValueError:
            raise UsageError("Invalid -a value, use -a NAME=VALUE", print_help=False)
        if opts.output:
            if opts.output == '-':
                self.settings.overrides['FEED_URI'] = 'stdout:'
            else:
                self.settings.overrides['FEED_URI'] = opts.output
            self.settings.overrides['FEED_FORMAT'] = opts.output_format
            self.settings.overrides['SESSION_POOL_SIZE'] = int(opts.pool)

    def run(self, args, opts):
        if not len(args):
            spiders = settings['DEFAULT_SPIDERS']
        else:
            spiders = args
        
        log.start()
        since = opts.since if datetime.strptime(opts.since, DATE_FORMAT) else opts.since
        until = opts.until
        self.scrapyd = Scrapyd(name='scrapyd')
        self.scrapyd.start()
        self.scrapyd.started.wait()
        for spider in spiders:
            enqueue(subspider(spider,
                                setting='FEED_URI=%s'%self.settings['FEED_URI'],
                                since=since,
                                until=until
                                ),
                    None)