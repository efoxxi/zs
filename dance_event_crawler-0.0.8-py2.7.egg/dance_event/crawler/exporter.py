
from scrapy.contrib.exporter import BaseItemExporter
from scrapy.item import Item


class OmitItemException(Exception):
    pass

class AlchemyItemExporter(BaseItemExporter):
    export_empty_fields = False
    fields_to_export = None
    encoding = 'utf-8'
    def __init__(self, queue, **kwargs):
        self.queue = queue
        
    def export_item(self, item):
        self.queue.put((item, 
                        lambda i:dict(self._get_serialized_fields(i))))