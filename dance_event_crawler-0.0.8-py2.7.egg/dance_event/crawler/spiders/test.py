import time

from multiprocessing import Value
from scrapy.spider import BaseSpider
from scrapy.item import Item, Field


from sqlalchemy import Integer, Column
from sqlalchemy.ext.declarative import declarative_base, AbstractConcreteBase


class StampBase(declarative_base()):
    __abstract__ = True
    _id = Column('id', Integer, primary_key=True)

    def __init__(self, **fields):
        for name, value in fields.iteritems():
            setattr(self, name, value)
        super(StampBase, self).__init__()


class StampModel(StampBase):
    __tablename__ = 'stamps'
    stamp = Column(Integer)


class Stamp(Item):
    _model = StampModel
    stamp = Field()


class TestSpider(BaseSpider):
    name = 'test-spider'
    start_urls = ['http://google.com',]
    counter = Value('i', 0)
    def parse(self, response):
        for i in xrange(1000):
            self.counter.value = self.counter.value+1
            yield Stamp(stamp=self.counter.value)