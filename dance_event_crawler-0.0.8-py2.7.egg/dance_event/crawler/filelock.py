import os
import time
from multiprocessing import Event

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from zc.lockfile import _lock_file, _unlock_file, LockError

class LockFile(object):
    def __init__(self, path):
        self._path = path
        fp = open(path, 'w+')
        _lock_file(fp)
        self._fp = fp
        fp.write(" %s\n" % os.getpid())
        fp.truncate()
        fp.flush()

    def close(self):
        _unlock_file(self._fp)
        self._fp.close()

class TruncatedLockFile(LockFile):
    def close(self):
        if hasattr(self, '_fp'):
            self._fp.truncate(0)
            self._fp.flush()
            super(TruncatedLockFile, self).close()

class Unlocked(FileSystemEventHandler):
    def __init__(self, path):
        self.event = Event()
        self.path = path + '.mplock'

    def _lock(self, path):
        lock = TruncatedLockFile(path)
        self.event.set()
        return lock

    def _unlock(self):
        self.lock.close()
        self.event.clear()

    def on_modified(self, event):
        try:
            if os.path.samefile(event.src_path, self.path):
                try:
                    self.lock = self._lock(self.path)
                except LockError:
                    pass
        except OSError:
            pass

    def __enter__(self):
        try:
            self.lock = self._lock(self.path)
        except LockError:
            if not hasattr(self, 'observer'):
                self.observer = Observer()
            self.observer.schedule(self,
                                   path=os.path.dirname(self.path),
                                   recursive=False)
            self.observer.start()

        if not self.event.is_set():
            self.event.wait()

    def __exit__(self, exc_type, exc_value, traceback):
        try:
            if hasattr(self, 'observer'):
                self.observer.stop()
                self.observer.join()
            if exc_value:
                raise exc_value
        finally:
            self._unlock()