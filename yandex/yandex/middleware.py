class SpiderDefinedMiddleware(object):
    def process_request(self, request, spider):
        if hasattr(spider, 'headers'):
            request.headers.update(spider.headers)