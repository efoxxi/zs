import os

from scrapy.http.cookies import CookieJar
from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from yandex.items import YandexItem
from scrapy.http import Request

def try_remove_file(file_name):
    print file_name
    try:
        os.remove(file_name)
    except os.error:
        pass

def write_to_file(file_name,response):
    fbody = open(file_name, 'ab')
    fbody.write(response.body)
    fbody.close()
    

class YandexSpider(CrawlSpider):
    name = 'yandex'
    allowed_domains = ['dance.zsconcepts.com']
#    start_urls = ['http://dance.zsconcepts.com/results/results_list.cgi',
#                  #'http://dance.zsconcepts.com/results/results_list.cgi'
#                  ]
    download_delay = 0.5

    headers_val = {'Accept-Language': ['en'],
                   'Accept-Encoding': ['x-gzip,gzip,deflate'],
                   'Accept': ['text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'],
                   'User-Agent': ['crawler/1.0'],
                   'Cookie': ['session_id=868602'],
                   'Referer': ['http://www.dance.zsconcepts.com/results/']
                   }
    
    filenames = ['___1_Competitions1.htm',
                 '___2_Competitions2.htm',
                 '___3_Competition_links.htm',
                 '___4_Scoresheets.htm',
                 '___5_Events.htm']
    for file_name in filenames:
        try_remove_file(file_name)


#    rules = (
#             Rule(SgmlLinkExtractor(), callback='parse_competitions'),
#    )

#    def parse_start_url(self, response):
#        self.parse_competitions(response)

    def start_requests(self):
        request = Request("http://www.dance.zsconcepts.com/results/",
                          headers=self.headers_val,
                          callback=self.parse_competitions1)
        yield request
    
    def parse_competitions1(self, response):
        write_to_file('___1_Competitions1.htm',response)
        request = Request("http://www.dance.zsconcepts.com/results/results_list.cgi",
                          headers=self.headers_val,
                          callback=self.parse_competitions2)
        yield request
        

    def parse_competitions2(self, response):
        write_to_file('___2_Competitions2.htm',response)
        
        request = Request("http://www.dance.zsconcepts.com/results/valpo2012/",
                          headers=self.headers_val,
                          callback=self.parse_competition_links)
        yield request

    def parse_competition_links(self, response):
        write_to_file('___3_Competition_links.htm',response)
        request = Request("http://www.dance.zsconcepts.com/results/valpo2012/Scoresheets.htm",
                          headers=self.headers_val,
                          callback=self.parse_events)
        yield request
    
    def parse_events(self, response):
        write_to_file('___4_Events.htm',response)
        pass