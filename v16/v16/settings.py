# Scrapy settings for v16 project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'v16'

SPIDER_MODULES = ['v16.spiders']
NEWSPIDER_MODULE = 'v16.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'v16 (+http://www.yourdomain.com)'
